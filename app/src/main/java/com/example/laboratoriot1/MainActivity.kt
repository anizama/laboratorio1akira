package com.example.laboratoriot1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 1 generamos evento en el boton verificar
        btnverificar.setOnClickListener {
            val anio = edtnacimiento.text.toString().toInt() //transformar de String a entero
            if (anio >=1930 && anio<=1948){ //  if (anio in 1969..1980){
                tvresultado.text="6.300,000"
                img1.setImageResource(R.drawable.austeridad)
            }
            else if (anio >=1949 && anio<=1968) {
                tvresultado.text="12.200,000"
                img1.setImageResource(R.drawable.ambicion)
            }
            else if (anio >=1969 && anio<=1980) {
                tvresultado.text="9.300,000"
                img1.setImageResource(R.drawable.exito)
            }
            else if (anio >=1981 && anio<=1993) {
                tvresultado.text="7.200,000"
                img1.setImageResource(R.drawable.frustacion)
            }
            else if (anio >=1994 && anio<=2010) {
                tvresultado.text="7.800,000"
                img1.setImageResource(R.drawable.irreverencia)
        }
            else if (anio >2010 || anio<1949) {
                tvresultado.text="No hay valores exactos para determinar la poblacion"
                img1.setImageResource(R.drawable.error)

    }
    }
}
}


